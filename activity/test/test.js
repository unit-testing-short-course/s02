const {checkName} = require('../src/util.js');
const {checkAge} = require('../src/util.js');
const {assert} = require('chai');


describe('test_checkName', () => {
	it('name_is_string', () => {
		const name = 0;
		assert.isString(checkName(name));
	})

	it('name_not_empty', () => {
		const name = "";
		assert.isNotEmpty(checkName(name));
	})

	it('name_not_undefined', () => {
		const name = undefined;
		assert.isDefined(checkName(name));
	})
	it('name_not_null', () => {
		const name = null;
		assert.isNotNull(checkName(name));
	})
	it('name_is_not_0', () => {
		const name = '';
		assert.notEqual(checkName(name).length, 0);
	})

})


describe('test_checkAge', () => {
	it('age_is_integer', () => {
		const age = "20";
		console.log(typeof(age))
		assert.isNotNumber(checkAge(age));
	})

	it('age_not_empty', () => {
		const age = "";
		assert.isNotEmpty(checkAge(age));
	})

	it('age_not_undefined', () => {
		const age = undefined;
		assert.isDefined(checkAge(age));
	})
	it('age_not_null', () => {
		const age = null;
		assert.isNotNull(checkAge(age));
	})
	it('age_is_not_0', () => {
		const age = 0;
		assert.notEqual(checkAge(age), 0);
	})

})