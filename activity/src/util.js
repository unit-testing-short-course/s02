function checkName(name){

	if(name == ""){
		return 'error: age field required'
	}
	else if (name === undefined){
		return 'error: age is undefined'
	}
	else if (name === null){
		return 'error: age is null'
	}
	else if (typeof(name) !== 'string'){
		return 'error: age is not number'
	}
	else if (name.length == 0){
		return 'error: age cannot have 0 characters'
	}

	return name;
}

function checkAge(age){

	if(age == ""){
		return 'error: age field required'
	}
	else if (age === undefined){
		return 'error: age is undefined'
	}
	else if (age === null){
		return 'error: age is null'
	}
	else if (typeof(age) !== 'number'){
		return 'error: age is not number'
	}
	else if (age <= 0){
		return 'error: age cannot be lower than 0'
	}

	return age;
}

module.exports = {
	checkName: checkName,
	checkAge: checkAge
}