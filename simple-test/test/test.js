const {checkEmail} = require('../src/util.js');
const {assert} = require('chai');


// is empty, is null. is undefined, is string
describe('test_checkEmail', () => {
	it('email_not_empty', () => {
		const email = "";
		assert.isNotEmpty(checkEmail(email));
	})

	it('email_not_undefined', () => {
		const email = undefined;
		assert.isDefined(checkEmail(email));
	})
	it('email_not_null', () => {
		const email = null;
		assert.isNotNull(checkEmail(email));
	})
	it('email_is_string', () => {
		const email = 10;
		assert.isString(checkEmail(email));
	})

})