function checkEmail(email){

	if(email == ""){
		return 'error: email field required'
	}
	else if (email === undefined){
		return 'error: email is undefined'
	}
	else if (email === null){
		return 'error: email is null'
	}
	else if (typeof(email) !== 'string'){
		return 'error: email is not string'
	}

	return email;
}

module.exports = {
	checkEmail: checkEmail
}